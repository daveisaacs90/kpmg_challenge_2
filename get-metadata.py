import requests
import sys
import json
import pprint
# Opening JSON file
f = open('metadata.json')
key = sys.argv[1]
# returns JSON object as 
# a dictionary
data = json.load(f)
if key == "Null":
    pprint.pprint(data)
else:
    meta_list = []
    for item in data:
        meta_details = {key :None}
        meta_details[key] = item[key]
        meta_list.append(meta_details)
    print(meta_list)